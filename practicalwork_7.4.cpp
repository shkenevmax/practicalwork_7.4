﻿// practicalwork_7.4.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <set>
#include <vector>
#include <algorithm>

using namespace std;

void push_back(int*& arr, int& size, const int val)
{
    int* newArr = new int[size + 1];
    for (int i = 0; i < size; i++) { newArr[i] = arr[i]; }
    newArr[size++] = val;
    delete[] arr;
    arr = newArr;
}

int main()
{
    // std::partial_sort_copy для массива, созданного при помощи рассмотренной в предыдущем модуле функции generate
    cout << "1. partial_sort_copy" << endl;
    cout << "Test array" << endl;
    int TestArray[10];
    int TempArray[10];
    srand(100);
    generate(begin(TestArray), end(TestArray), []() {return rand()%10; });
    for (int i = 0; i < 9; i++) { cout << TestArray[i] << endl; }
    partial_sort_copy(begin(TestArray), end(TestArray), begin(TempArray), end(TempArray));
    cout << "Temp array" << endl;
    for (int i = 0; i < 9; i++)
    {
        cout << TempArray[i] << endl;
    }
    cout << endl;

    // lower_bound, upper_bound, set::lower_bound, set::upper_bound — для целочисленного массива из 10 элементов
    cout << "2. lower_bound, upper_bound" << endl;
    set<int> myset;
    set<int>::iterator itlow, itup;
    for (int i = 1; i < 10; i++) myset.insert(i);
    itlow = myset.lower_bound(3);
    itup = myset.upper_bound(6);
    myset.erase(itlow, itup);
    cout << "Myset contains:" << endl;
    for (set<int>::iterator it = myset.begin(); it != myset.end(); ++it) { cout << *it << endl; }
    cout << endl;

    // includes и set_difference — для поиска вхождения в сортированное множество другого (меньшего) 
    // сортированного множества и переноса отличающихся элементов 
    cout << "3. includes & set_difference" << endl;
    int arrSize = 4;
    int* FullArray{ new int[10] { 5, 10, 15, 20, 25, 30, 35, 40, 45, 50 } };
    int* IncompleteArray{ new int[arrSize] {  40,30,20,10 } };
    vector<int> vec(10);
    vector<int>::iterator iter;
    sort(FullArray, FullArray + 10);
    sort(IncompleteArray, IncompleteArray + 4);
    if (includes(FullArray, FullArray + 10, IncompleteArray, IncompleteArray + 4))
    {
        iter = set_difference(FullArray, FullArray + 10, IncompleteArray, IncompleteArray + 4, vec.begin());
    }
    cout << "Incomplete array: " << endl;
    for (iter = vec.begin(); iter != vec.end(); ++iter)
    {
        if (*iter != 0) push_back(IncompleteArray, arrSize, *iter);
    }
    for (int i = 0; i < arrSize; i++) { cout << IncompleteArray[i] << endl; }
}